export default interface Employee {
  Education: string
  JoiningYear: number
  City: string
  PaymentTier: number
  Age: number
  Gender: string
  EverBenched: string
  ExperienceInCurrentDomain: number
}
